﻿#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Car
{
private:
    string color;
    int speed;
    double x = 0;
    double y = 0;
    double z = 0;

public:
    void AddParameter(string Acolor, int Aspeed)
    {
        color = Acolor;
        speed = Aspeed;
    }

    string GetColor()
    {
        return color;
    }

    int GetSpeed()
    {
        return speed;
    }

    // Метод класса для определения модуля вектора
    double VectorModule(double _x, double _y, double _z)
    {
        x = _x;
        y = _y;
        z = _z;
        double Module = sqrt(_x * _x + _y * _y + _z * _z);
        cout << "Длина вектора равна: " << Module << endl;
        return Module;

    };
};

int main()
{
    setlocale(LC_ALL, "RU");

    /*1.Создать класс со своими данными, скрытыми при помощи private.
    Сделать public метод для вывода этих данных. Протестировать.*/
    Car BMW;
    BMW.AddParameter("black", 300);
    cout << "Цвет машины: " << BMW.GetColor() << endl
        << "Скорость машины: " << BMW.GetSpeed() << endl;

    /*Дополнить класс Vector public методом, который будет
    возвращать длину (модуль) вектора. Протестировать.*/
    Car V;
    V.VectorModule(10.5, 12.4, -2.13);

    return 0;
}